<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
</head>
<body>
	<p id="demo4">버튼을 누르면 좌표가 업데이트 됩니다.:</p>
	<button onclick="showPosition4()">좌표 구하기!!</button>
	<p id="text1"></p>
	<p id="text2"></p>

	<script type="text/javascript"
		src="http://maps.googleapis.com/maps/api/js?sensor=true">
		
	</script>
	<script>
	
		var x4 = document.getElementById("demo4");
		/* 
		function getLocation4() {
			if (navigator.geolocation) {
				//브라우져가 geolocation을 지원하는지 확인한 후 
				navigator.geolocation.watchPosition(showPosition4);
				//지원할 경우에 watchPosition 메소드를 실행합니다. watchPosition은 변화되는 좌표를 계속적으로 업데이트 해줍니다. getCurrentPosition은 단 한번 좌표를 알려줍니다.
			} else {
				x4.innerHTML = "이 브라우저는 위치추적이 지원되지 않습니다.";
				//지원되지 않는 브라우져의 경우에는 사용자에게 알려주게 됩니다. 여기서 x4는 위에서 얻어온 demo4문단입니다.
			}
		} */
		
		var x = new Array();
		var y = new Array();
		
		x[0]=37.574556;
		y[0]=126.997833;
		
		function showPosition4() {
			
			var latlng = new Array();
			var forAddress, forLatlng, geocoder = "";
			
			for (var i = 0; i < x.length; i++) {
				latlng[i] = new google.maps.LatLng(x[i], y[i]);
				forLatlng = document.getElementById("text1");
				forAddress = document.getElementById("text2");
				forLatlng.innerHTML = ": Latitude: " + x[i] + "_$tag_Longitude: " + y[i];
				geocoder = new google.maps.Geocoder();
				geocoder.geocode(
								{
									'latLng' : latlng[i]
								},
								function(results, status)
								{
									if (status == google.maps.GeocoderStatus.OK) {
										if (results[1]) {
											forAddress.innerHTML = results[3].formatted_address;
											
										}

									} else {
										alert("Geocoder failed due to: "+ status);
									}
				});
			}
		}
	</script>
</body>

</html>