<%@ page language="java" contentType="text/html; charset=UTF-8" errorPage ="DBError.jsp"
    pageEncoding="UTF-8"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<link type="text/css" rel="stylesheet" href="./css/Plaza.css"/>
<script type="text/javascript" src="./js/Plaza.js"></script>                   
<link href="./image/mark.ico" rel="shortcut icon" />
</head>
<body>
	<script>
		<%
			String contents [] = (String[]) request.getAttribute("content");
			String FBNames [] = (String[]) request.getAttribute("FBname");
			String FBpaths [] = (String[]) request.getAttribute("FBpath");
			String savedays [] = (String[]) request.getAttribute("saveday");
			String savetimes [] = (String[]) request.getAttribute("savetime");
			String ids [] = (String[]) request.getAttribute("ids");
		%>
		
		var content = Array();
		var path = Array();
		var userName = Array();
		var days = Array();
		var times = Array();
		var ids = Array();
		
		<%for(int i = 0; i < contents.length;  i++) { %>
	   	 	content.push('<%=contents[i]%>');
		   	path.push('<%=FBpaths[i]%>');
		   	userName.push('<%=FBNames[i]%>');
		   	days.push('<%=savedays[i]%>');
		   	times.push('<%=savetimes[i]%>');
		   	ids.push('<%=ids[i]%>');
	    <%}%>
	    
	    
	</script>
	
	<div id="DicBody" style="overflow: hidden; width: 100%; height:100%">
	<div class = "downPage"></div>
	
	 <jsp:include page="header.html" />
	
    <div id="map_canvas" style="width:100%; height:80%;">
    	<div id="writing" style="width:100%; height:13%; margin: 10px; float: center;">
    	<form name='myForm'  method='get'>
    		<textarea id="msg" class="textarea" name="PlazaContent"></textarea>
    		<input type='image' src="./image/sendBtn.png" style='width:8%;' class="write" onClick="uploadText()">
    	</form>
    	</div>
    	<span style="float:right; margin: 30px"></span>
			<span class="label">첨부</span>
			<select name="attType" id="attachType" onchange="myRoute(this.value)">
				<option value="">첨부하기</option>
				<option value="route">루트</option>
			</select>
    	<div id="perpectiveT" style="width:20%; margin: 10px"></div> 
		<hr>
		<div id="boards" style="width:800px;">
		<script>
			while(true) {
				for(var i = content.length-1; i >= 0 ; i--){
					document.write("<tr><div id='post' class='board1'>");
					document.write("<form name='myForm1' method='get' action='./EncodingPlazaDelete.jsp'><input type='image' style='float:right; height:10px; width:10px;' "+
							"src='./image/deleteBtn.png' onClick='deleteButton("+i+")'></form>");
					console.log("i: "+i);
					document.write("<div class='board2'>");
					document.write("<input type=image src="+path[i]+">");
					document.write("</div>");
					document.write("<div style='width:510px' > <ul class='unstyled' style='width:100%'>");
					document.write("<li><h6>"+userName[i]+"&nbsp;</h6></li><li>");
					document.write(content[i]);
					document.write("</li></ul></div></div></tr>");
				}
				break;
			}
			
		</script>
		</div>
    </div>
    </div>
   <div class="container" style="padding:10px; background:#FF4848;">
		<div class="container" style="padding:10px;"></div>
		<p style='text-align:center; color:white; font-weight:bold;'>&nbsp; &nbsp; &nbsp; &nbsp; &copy; 2014 Travelo Project Team,Inc. All Rights Reserved.</p>
		
	</div>

</body>
</html>