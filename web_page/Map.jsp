<%@ page language="java" contentType="text/html; charset=UTF-8"
	errorPage="DBError.jsp" pageEncoding="UTF-8"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<jsp:include page="header.html" />
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />

<style type="text/css">
html {
	height: 100%
}

body {
	height: 100%;
	margin: 0;
	padding: 10px;
}

#map_canvas {
	height: 100%;
}
</style>

<script type="text/javascript"
	src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCcJvhYXl7bMrWkUgPkw6N07EEA0bdyY_k&sensor=TRUE">
    </script>



<script type="text/javascript">
    
    //var photoIcon = new google.maps.MarkerImage("./image/photo.png",null,null,null,new google.maps.Size(30,30));
    
    <% 
    	String resultLogX [] = (String[])request.getAttribute("logx1"); 
       	String resultLogY [] = (String[])request.getAttribute("logy1"); 
    %>
    
    var x = new Array();
    var y = new Array();
    
    <%for(int i = 0; i < resultLogX.length;  i++) { %>
   	 x.push('<%=resultLogX[i]%>');
   	 y.push('<%=resultLogY[i]%>');
    <%}%>
	
    sessionStorage.setItem("logx",x);
    sessionStorage.setItem("logy",y);

    
    var latIng = [];
    
    //var pathPosition = [new google.maps.LatLng(x[0], y[0]), new google.maps.LatLng(x[1], y[1])];
    var photoURL = "./image/photo.png";
    var contentString = '<div id = "content">' + '</div>'+'<img src = '+photoURL+'/>';
    
    function initialize() {
    	for(var i = 0; i < x.length;i++) {
			latIng[i] = new google.maps.LatLng(x[i], y[i]);
		}
    	
    	var mapOptions = {
		    center: latIng[0],
		    zoom: 15,
		    mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		                
		var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
		
		for(var i = 0; i < x.length; i++) {
			var marker = new google.maps.Marker({
		      	map: map,
		       	position: new google.maps.LatLng(x[i], y[i]),
		       	//icon: photoIcon,
		       	title: "사진입니다."
			});
		  
			var infoWindow = new google.maps.InfoWindow({
				content: contentString
			});
		
			google.maps.event.addListener(marker, 'click', (function(marker, i) {
				return function() {
					infoWindow.open(map,marker);
				};
			})(marker, i));
		}
		
		
			var flightPath = new google.maps.Polyline({
		 	   	path: latIng,
			    geodesic: true,
		  	 	strokeColor: '#FF0000',
		  	 	strokeOpacity: 1.0,
		   	 	strokeWeight: 2
			});
			flightPath.setMap(map);
		  
	}
      
    google.maps.event.addDomListener(window, 'load', initialize);  
  
      
    </script>
</head>
<div class="downPage"></div>
<body onload="initialize()">
	<div id="blank" style="padding: 15px;">	</div>
	<div id="map_canvas" style="width: 100%; height: 80%;"></div>
	<div class="container" style="padding:10px; background:#FF4848;">
		<div class="container" style="padding:10px;"></div>
		<p style='text-align:center; color:white; font-weight:bold;'>&nbsp; &nbsp; &nbsp; &nbsp; &copy; 2014 Travelo Project Team,Inc. All Rights Reserved.</p>
		
	</div>
</body>
</html>